package com.jaom.ws.soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WswsdlfirstwsApplication {

	public static void main(String[] args) {
		SpringApplication.run(WswsdlfirstwsApplication.class, args);
	}

}
